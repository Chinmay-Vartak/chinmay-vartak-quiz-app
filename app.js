(function() {
    var questions = 
    [
     {question: "What is colour of apple ?", choices: ["red", "blue", "pink", "grey", "black", "indigo"], correctAnswer: 0}, 
     {question: "What is 17 + 1?",choices: [3, 6, 12, 18],correctAnswer: 3}, 
     {question: "What is capital of Maharashtra",choices: ["Mumbai", "Bengaluru", "Delhi"],correctAnswer: 0},
     {question: "What is 1*4?",choices: [9, 5, 6, 4, 8],correctAnswer: 3}, 
     {question: "What is full form of IoT",choices: ["Internet of Talent", "Internet of Things", "Ice-cream of Talent", "Integration of Telephones", "None"],correctAnswer: 1}
    ];
    var questionCounter = 0;      //Tracks question number
    var selections = [];          //Array containing user choices
    var quiz = $('#quiz');        //Quiz div object

    displayNext(); //-------- initial question (init =0)

    
    $('#next').on('click', function (e) {    // Click on 'next' button
      e.preventDefault();
      choose();
      //  no user selection == stopped 
      if (isNaN(selections[questionCounter]))  //  no user selection == stopped
      {
        alert('Cannot move ahead without Attempting');
      }
       else 
      {
        questionCounter++;
        displayNext();
      }
    });

    
    $('#prev').on('click', function (e) {     // Click on 'prev' button
      e.preventDefault();   // --------- save prev ans
      choose();
      questionCounter--;
      displayNext();
    });

    
    $('#start').on('click', function (e) {    // Click  'Start Over' button
      e.preventDefault();
      questionCounter = 0;   // ------ till 1st qs only
      selections = [];
      displayNext();
      $('#start').hide();
    });


    $('.button').on('mouseenter', function () {        // hover mouse on button
      $(this).addClass('active');
    });
    $('.button').on('mouseleave', function () {
      $(this).removeClass('active');
    });


    function createQuestionElement(index) {
      var qElement = $('<div>', {
        id: 'question'
      });
      
      var header = $('<h2>Question ' + (index + 1) + ':</h2>');
      qElement.append(header);
      
      var question = $('<p>').append(questions[index].question);
      qElement.append(question);
      
      var radioButtons = createRadios(index);
      qElement.append(radioButtons);
      
      return qElement;
    }


    function createRadios(index) {            // make list of answer as radio inputs
      var radioList = $('<ul>');
      var item;
      var input = '';
      for (var i = 0; i < questions[index].choices.length; i++) {
        item = $('<li>');
        input = '<input type="radio" name="answer" value=' + i + ' />';
        input += questions[index].choices[i];
        item.append(input);
        radioList.append(item);
      }
      return radioList;
    }

    
    function choose() {      // pushes the answer to an array
      selections[questionCounter] = +$('input[name="answer"]:checked').val();
    }


    function displayNext() {       // Displays next qs
      quiz.fadeOut(function() {
        $('#question').remove();
        if(questionCounter < questions.length){
          var nextQuestion = createQuestionElement(questionCounter);
          quiz.append(nextQuestion).fadeIn();
          if (!(isNaN(selections[questionCounter]))) {
            $('input[value='+selections[questionCounter]+']').prop('checked', true);
          }
          // Controls display of 'prev' button
          if(questionCounter === 1){
            $('#prev').show();
          } else if(questionCounter === 0){
            
            $('#prev').hide();
            $('#next').show();
          }
        }else {
          var scoreElem = displayScore();
          quiz.append(scoreElem).fadeIn();
          $('#next').hide();
          $('#prev').hide();
          $('#start').show();
        }
      });
    }

    
    function displayScore() {  // Computes score 
      var score = $('<p>',{id: 'question'});
      var numCorrect = 0;
      for (var i = 0; i < selections.length; i++) 
      {
        if (selections[i] === questions[i].correctAnswer) 
        {
          numCorrect++;
        }
      }
      score.append('Your Score is ' + numCorrect + ' out of  ' + questions.length );
      return score;
    }
  })
();
